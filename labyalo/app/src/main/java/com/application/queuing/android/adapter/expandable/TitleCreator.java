package com.application.queuing.android.adapter.expandable;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class TitleCreator {

    static TitleCreator _titleCreator;
    List<TitleParent> _titleParents;

    public TitleCreator(Context context, ArrayList<String> category) {
        _titleParents = new ArrayList<>();
        for(int i=0;i< category.size();i++)
        {
            TitleParent title = new TitleParent(category.get(i));
            _titleParents.add(title);
        }
    }

    public TitleCreator(Context context){

    }

    public static TitleCreator get(Context context, ArrayList<String> category)
    {
        if(_titleCreator == null)
            _titleCreator = new TitleCreator(context, category);
        return _titleCreator;
    }

    public List<TitleParent> getAll() {
        return _titleParents;
    }
}