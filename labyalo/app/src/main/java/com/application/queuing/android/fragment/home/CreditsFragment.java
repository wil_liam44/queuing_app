package com.application.queuing.android.fragment.home;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.application.queuing.R;
import com.application.queuing.android.activity.HomeActivity;
import com.application.queuing.android.adapter.TransactionsRecyclerViewAdapter;
import com.application.queuing.data.model.api.SampleModel;
import com.application.queuing.server.request.Auth;
import com.application.queuing.vendor.android.base.BaseFragment;
import com.application.queuing.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CreditsFragment extends BaseFragment implements View.OnClickListener{

    private HomeActivity homeActivity;

    private TransactionsRecyclerViewAdapter defaultRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    @BindView(R.id.transactionsRV)          RecyclerView transactionsRV;
    @BindView(R.id.backIcon)                ImageView back;

    public static CreditsFragment newInstance(){
        CreditsFragment creditsFragment = new CreditsFragment();
        return creditsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_credits;
    }

    @Override
    public void onViewReady() {
        homeActivity = (HomeActivity) getContext();
        back.setOnClickListener(this);
        setUpRecyclerView();
    }

    public void setUpRecyclerView(){
        defaultRecyclerViewAdapter = new TransactionsRecyclerViewAdapter(getContext());
        defaultRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager = new LinearLayoutManager(getContext());
        transactionsRV.setLayoutManager(linearLayoutManager);
        transactionsRV.setAdapter(defaultRecyclerViewAdapter);
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.backIcon:
                homeActivity.onBackPressed();
                break;
            default:
                break;

        }
    }
}
