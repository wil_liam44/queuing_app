package com.application.queuing.android.fragment.home;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.application.queuing.R;
import com.application.queuing.android.activity.QueueDetailsActivity;
import com.application.queuing.android.adapter.QueuePicturesAdapater;
import com.application.queuing.android.adapter.expandable.MyAdapter;
import com.application.queuing.android.adapter.expandable.TitleChild;
import com.application.queuing.android.adapter.expandable.TitleCreator;
import com.application.queuing.android.adapter.expandable.TitleParent;
import com.application.queuing.android.dialog.JoinQueueDialog;
import com.application.queuing.data.model.api.SampleModel;
import com.application.queuing.server.request.Auth;
import com.application.queuing.vendor.android.base.BaseFragment;
import com.application.queuing.vendor.server.transformer.BaseTransformer;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class QueueFragment extends BaseFragment implements View.OnClickListener, JoinQueueDialog.Callback {
    public static final String TAG =QueueFragment.class.getName().toString();

    private QueueDetailsActivity queueActivity;
    private LinearLayoutManager linearLayoutManager;
    private QueuePicturesAdapater defaultRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager1;

    @BindView(R.id.myRecyclerView)          RecyclerView biddingsRV;
    @BindView(R.id.queuepicturesRV)         RecyclerView picturesQueuesRV;
    @BindView(R.id.joinQueueBTN)            LinearLayout joinBTN;
    @BindView(R.id.firstHere)               ImageView firstHere;
    @BindView(R.id.secondHere)               ImageView secondHere;
    @BindView(R.id.thirdHere)               ImageView thirdHere;
    @BindView(R.id.fourthHere)               ImageView fourthHere;
    @BindView(R.id.backIcon)                ImageView backIcon;

    public static QueueFragment newInstance(){
        QueueFragment queueFragment = new QueueFragment();
        return queueFragment;
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_queue_details;
    }

    @Override
    public void onViewReady() {
        queueActivity = (QueueDetailsActivity) getContext();
        joinBTN.setOnClickListener(this);
        firstHere.setVisibility(View.INVISIBLE);
        secondHere.setVisibility(View.INVISIBLE);
        thirdHere.setVisibility(View.INVISIBLE);
        fourthHere.setVisibility(View.INVISIBLE);
        backIcon.setOnClickListener(this);
        setUpExpandableRecycler();
        setUpRecylcer();
    }

    public void setUpExpandableRecycler(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        biddingsRV.setLayoutManager(linearLayoutManager);

        TitleCreator titleCreator =TitleCreator.get(getContext(), getBids());
        List<TitleParent> titles = titleCreator.getAll();
        List<ParentObject> parentObject = new ArrayList<>();
        for(TitleParent title:titles)
        {
            List<Object> childList = new ArrayList<>();
            ArrayList<SampleModel> photosChild = new ArrayList<>();
            SampleModel defaultItem;
            for(int i = 0; i < 5; i++) {
                defaultItem = new SampleModel();
                defaultItem.id = i;
                defaultItem.name = "name " + i;
                photosChild.add(defaultItem);

            }
            childList.add(new TitleChild(photosChild));
            title.setChildObjectList(childList);
            parentObject.add(title);
        }
        MyAdapter adapter = new MyAdapter(getContext(),parentObject);
        adapter.setParentClickableViewAnimationDefaultDuration();
        adapter.setParentAndIconExpandOnClick(true);

        biddingsRV.setAdapter(adapter);
    }

    public void setUpRecylcer(){
        defaultRecyclerViewAdapter = new QueuePicturesAdapater(getContext());
        defaultRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager1 = new LinearLayoutManager(getContext(), LinearLayout.HORIZONTAL, false);
        picturesQueuesRV.setLayoutManager(linearLayoutManager1);
        picturesQueuesRV.setAdapter(defaultRecyclerViewAdapter);
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }

    public  ArrayList<String> getBids(){
        java.util.ArrayList<String> bids = new ArrayList<>();

        bids.add("Buy");
        bids.add("Sell");


        return bids;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.joinQueueBTN:
                JoinQueueDialog.newInstance(this).show(getFragmentManager(), TAG);
                break;
            case R.id.backIcon:
                queueActivity.onBackPressed();
                break;
        }


    }

    @Override
    public void onSuccess(String sample) {
        Toast.makeText(getContext(), sample, Toast.LENGTH_LONG).show();
        if(sample.equalsIgnoreCase("one")) {
            firstHere.setVisibility(View.VISIBLE);
            secondHere.setVisibility(View.INVISIBLE);
            thirdHere.setVisibility(View.INVISIBLE);
            fourthHere.setVisibility(View.INVISIBLE);
        }
        else if(sample.equalsIgnoreCase("two")) {
            secondHere.setVisibility(View.VISIBLE);
            firstHere.setVisibility(View.INVISIBLE);
            thirdHere.setVisibility(View.INVISIBLE);
            fourthHere.setVisibility(View.INVISIBLE);
        }
        else if(sample.equalsIgnoreCase("three")) {
            thirdHere.setVisibility(View.VISIBLE);
            secondHere.setVisibility(View.INVISIBLE);
            firstHere.setVisibility(View.INVISIBLE);
            fourthHere.setVisibility(View.INVISIBLE);
        }
        if(sample.equalsIgnoreCase("four")) {
            fourthHere.setVisibility(View.VISIBLE);
            secondHere.setVisibility(View.INVISIBLE);
            thirdHere.setVisibility(View.INVISIBLE);
            firstHere.setVisibility(View.INVISIBLE);
        }
    }
}
