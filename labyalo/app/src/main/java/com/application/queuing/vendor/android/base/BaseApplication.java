package com.application.queuing.vendor.android.base;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.application.queuing.R;
import com.application.queuing.config.App;
import com.application.queuing.data.preference.Data;
import com.application.queuing.vendor.android.java.Log;

import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Labyalo on 3/16/2017.
 */

public class BaseApplication extends Application {

    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Realm.init(context);

        if(!App.debug){
            Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
            {
                @Override
                public void uncaughtException (Thread thread, Throwable e)
                {
                    Log.e("BaseApplication", e.getMessage());

                    System.gc();
                    Data.insert(Data.ERROR_COUNT, Data.getInt(Data.ERROR_COUNT) + 1);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle(getString(R.string.app_name))
                                    .setContentText("App restarted due to some error.");

                    NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    mNotifyMgr.notify(001, mBuilder.build());

//                    RouteManager.Route.with(context)
//                            .addActivityClass(LandingActivity.class)
//                            .addActivityTag("registration")
//                            .addFragmentTag("splash")
//                            .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    System.exit(1);
                }
            });
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.typeface_regular))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public void dd(String message){
        dd("Default", message);
    }

    public void dd(String tag, String message){
        Log.dd(tag, message);
    }
}
