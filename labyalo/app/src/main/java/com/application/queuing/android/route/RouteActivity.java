package com.application.queuing.android.route;

import android.content.Intent;

import com.application.queuing.android.activity.HomeActivity;
import com.application.queuing.android.activity.LandingActivity;
import com.application.queuing.android.activity.QueueDetailsActivity;
import com.application.queuing.android.activity.RegisterActivity;
import com.application.queuing.vendor.android.base.BaseActivity;
import com.application.queuing.vendor.android.base.RouteManager;

/**
 * Created by Labyalo on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(HomeActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startQueueActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(QueueDetailsActivity.class)
                .addActivityTag("queue")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startRegisterActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(RegisterActivity.class)
                .addActivityTag("register")
                .addFragmentTag(fragmentTAG)
                .startActivity();
                finish();
    }

    public void startLandingActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("landing")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

//    @Subscribe
//    public void invalidToken(InvalidToken invalidToken){
//        Log.e("Token", "Expired");
//        EventBus.getDefault().unregister(this);
//        UserData.insert(new UserModel());
//        UserData.insert(UserData.TOKEN_EXPIRED, true);
//        startLandingActivity();
//    }
}
