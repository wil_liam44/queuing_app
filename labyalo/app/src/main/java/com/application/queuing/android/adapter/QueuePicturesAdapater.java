package com.application.queuing.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.application.queuing.R;
import com.application.queuing.data.model.api.SampleModel;
import com.application.queuing.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class QueuePicturesAdapater extends BaseRecylerViewAdapter<QueuePicturesAdapater.ViewHolder, SampleModel> {

    private ClickListener clickListener;

    public QueuePicturesAdapater(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.picture_list));
    }

    @Override
    public void onBindViewHolder(QueuePicturesAdapater.ViewHolder holder, int position) {
        holder.setItem(getItem(position));

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON) View adapterCON;
        public ViewHolder(View view) {
            super(view);
        }

        public SampleModel getItem() {
            return (SampleModel) super.getItem();
        }
    }

    public void setClickListener(QueuePicturesAdapater.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(SampleModel sampleModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((SampleModel) v.getTag());
                }
                break;
        }
    }
}