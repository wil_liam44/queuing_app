package com.application.queuing.android.adapter.expandable;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.application.queuing.R;
import com.application.queuing.data.model.api.SampleModel;

import java.util.List;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ImageViewHolder>{


    private Context mContext;
    private List<SampleModel> mUploads;

    public ChildAdapter(Context context, List<SampleModel> uploads) {
        mContext = context;
        mUploads = uploads;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_childnode, parent, false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, final int position) {
//        Model_Upload uploadCurrent = mUploads.get(position);
//        holder.textViewName.setText(uploadCurrent.getName());
//        Picasso.with(mContext)
//                .load(uploadCurrent.getImageUrl())
//                .fit()
//                .centerCrop()
//                .into(holder.imageView);


        //holder.textViewName.setText(mUploads.get(position).());
    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewName;
        public ImageView imageView;

        public ImageViewHolder(View itemView) {
            super(itemView);


        }

    }
}