package com.application.queuing.data.model.api;

import com.application.queuing.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UserModel extends AndroidModel {
    @SerializedName("name")
    public String name;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public UserModel convertFromJson(String json) {
        return convertFromJson(json, UserModel.class);
    }
}
