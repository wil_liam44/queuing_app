package com.application.queuing.android.activity;

import android.view.View;

import com.application.queuing.R;
import com.application.queuing.android.fragment.home.QueueFragment;
import com.application.queuing.android.route.RouteActivity;

public class QueueDetailsActivity extends RouteActivity implements View.OnClickListener {

    private static final String TAG = QueueDetailsActivity.class.getName().toString();


    @Override
    public int onLayoutSet() {
        return R.layout.activity_queuing;
    }

    @Override
    public void onViewReady() {
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){

            case "queue":
                openQueueFragment();
                break;
            default:
                break;
        }


    }

    public void openQueueFragment(){switchFragment(QueueFragment.newInstance());}

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            default:
                break;

        }
    }
}
