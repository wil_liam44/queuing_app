package com.application.queuing.android.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.application.queuing.R;
import com.application.queuing.android.fragment.home.CreditsFragment;
import com.application.queuing.android.fragment.home.HomeFragment;
import com.application.queuing.android.fragment.home.ProfileFragment;
import com.application.queuing.android.fragment.landing.LoginFragment;
import com.application.queuing.android.fragment.register.SignUpFragment;
import com.google.android.gms.maps.GoogleMap;
import com.application.queuing.android.route.RouteActivity;

import butterknife.BindView;

public class HomeActivity extends RouteActivity implements View.OnClickListener{

    private static final String TAG = HomeActivity.class.getName().toString();

    private GoogleMap mMap;

    @BindView(R.id.homePanel)               LinearLayout homePanel;
    @BindView(R.id.historyPanel)            LinearLayout historyPanel;
    @BindView(R.id.AddQueuePanel)           LinearLayout addqueuePanel;
    @BindView(R.id.creditsPanel)            LinearLayout creditsPanel;
    @BindView(R.id.profilePanel)            LinearLayout profilePanel;
    @BindView(R.id.homeText)                TextView    homeText;
    @BindView(R.id.homeIcon)                ImageView   homeIcon;
    @BindView(R.id.historyText)             TextView    historyText;
    @BindView(R.id.historyIcon)             ImageView   historyIcon;
    @BindView(R.id.addQueueText)                TextView    addQueueText;
    @BindView(R.id.addQueueIcon)                ImageView   addQueueIcon;
    @BindView(R.id.creditsText)                TextView    creditsText;
    @BindView(R.id.creditsIcon)                ImageView   creditsIcon;
    @BindView(R.id.profileText)                TextView    profileText;
    @BindView(R.id.profileIcon)                ImageView   profileIcon;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {
        homePanel.setOnClickListener(this);
        historyPanel.setOnClickListener(this);
        addqueuePanel.setOnClickListener(this);
        creditsPanel.setOnClickListener(this);
        profilePanel.setOnClickListener(this);
    }

    public void openHomePanel(){
        homeIcon.setImageResource(R.drawable.home_clicked_icon);
        homeText.setTextColor(getResources().getColor(R.color.buttonColor));
        profileIcon.setImageResource(R.drawable.profile_icon);
        profileText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        creditsIcon.setImageResource(R.drawable.credits_icon);
        creditsText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        historyIcon.setImageResource(R.drawable.history_icon);
        historyText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
    }

    public void openCreditsPanel(){
        homeIcon.setImageResource(R.drawable.home_icon);
        homeText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        profileIcon.setImageResource(R.drawable.profile_icon);
        profileText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        creditsIcon.setImageResource(R.drawable.credits_clicked_icon);
        creditsText.setTextColor(getResources().getColor(R.color.buttonColor));
        historyIcon.setImageResource(R.drawable.history_icon);
        historyText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
    }

    public void openProfile(){
        homeIcon.setImageResource(R.drawable.home_icon);
        homeText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        profileIcon.setImageResource(R.drawable.profile_clicked_icon);
        profileText.setTextColor(getResources().getColor(R.color.buttonColor));
        creditsIcon.setImageResource(R.drawable.credits_icon);
        creditsText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        historyIcon.setImageResource(R.drawable.history_icon);
        historyText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "home":
                openHomeFragment();
                break;
            case "credits":
                openCreditsFragment();
                break;
            default:
        }


    }

    public void openCreditsFragment() {
        openCreditsPanel();
        switchFragment(CreditsFragment.newInstance());}
    public void openHomeFragment() {
        openHomePanel();
        switchFragment(HomeFragment.newInstance());}
    public void openProfileFragment(){
        openProfile();
        switchFragment(ProfileFragment.newInstace());
    }


    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.homePanel:
                openHomeFragment();
                break;
            case R.id.historyPanel:
                break;
            case R.id.AddQueuePanel:
                break;
            case R.id.creditsPanel:
                openCreditsFragment();
                break;
            case R.id.profilePanel:
                openProfileFragment();
                break;
            default:
                break;

        }
    }
}
