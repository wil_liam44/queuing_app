package com.application.queuing.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.application.queuing.R;
import com.application.queuing.data.model.api.SampleModel;
import com.application.queuing.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class TransactionsRecyclerViewAdapter extends BaseRecylerViewAdapter <TransactionsRecyclerViewAdapter.ViewHolder, SampleModel>{

    private DefaultRecyclerViewAdapter.ClickListener clickListener;

    public TransactionsRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.recyclerview_credit_transactions));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.userNameTXT)     TextView nameTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public SampleModel getItem() {
            return (SampleModel) super.getItem();
        }
    }

    public void setClickListener(DefaultRecyclerViewAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(SampleModel sampleModel);
    }
}
