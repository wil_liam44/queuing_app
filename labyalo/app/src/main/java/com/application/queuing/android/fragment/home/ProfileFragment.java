package com.application.queuing.android.fragment.home;

import android.util.Log;
import android.view.View;

import com.application.queuing.R;
import com.application.queuing.android.activity.HomeActivity;
import com.application.queuing.server.request.Auth;
import com.application.queuing.vendor.android.base.BaseFragment;
import com.application.queuing.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ProfileFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG =ProfileFragment.class.getName().toString();


    private HomeActivity homeActivity;

    public static ProfileFragment newInstace(){
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewReady() {
        homeActivity = (HomeActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {

    }
}
