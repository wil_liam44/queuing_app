package com.application.queuing.android.adapter.expandable;

import com.application.queuing.data.model.api.SampleModel;

import java.util.ArrayList;
import java.util.List;

public class TitleChild {

    public List<SampleModel> options = new ArrayList<>();

    public TitleChild(ArrayList<SampleModel> uploads) {
        options = uploads;
    }

    public void setOptions(List<SampleModel> options){
        this.options = options;
    }

    public List<SampleModel> getOptions(){
        return options;
    }
}