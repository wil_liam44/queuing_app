package com.application.queuing.android.adapter.expandable;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.application.queuing.R;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

public class TitleParentViewHolder extends ParentViewHolder {
    public TextView _textView;
    public ImageButton _imageButton;

    public TitleParentViewHolder(View itemView) {
        super(itemView);
        _textView = (TextView)itemView.findViewById(R.id.parentTitle);
        _imageButton = (ImageButton) itemView.findViewById(R.id.expandArrow);
    }
}