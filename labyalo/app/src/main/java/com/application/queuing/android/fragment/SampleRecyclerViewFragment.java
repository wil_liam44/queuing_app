package com.application.queuing.android.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.application.queuing.R;
import com.application.queuing.android.adapter.DefaultRecyclerViewAdapter;
import com.application.queuing.data.model.api.SampleModel;
import com.application.queuing.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SampleRecyclerViewFragment extends BaseFragment {
    public static final String TAG = SampleRecyclerViewFragment.class.getName().toString();

    private DefaultRecyclerViewAdapter defaultRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.defaultRV)  RecyclerView defaultRV;

    public static SampleRecyclerViewFragment newInstance() {
        SampleRecyclerViewFragment fragment = new SampleRecyclerViewFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_recylerview;
    }

    @Override
    public void onViewReady() {
        setUpListView();
    }

    private void setUpListView(){
        defaultRecyclerViewAdapter = new DefaultRecyclerViewAdapter(getContext());
        defaultRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager = new LinearLayoutManager(getContext());
        defaultRV.setLayoutManager(linearLayoutManager);
        defaultRV.setAdapter(defaultRecyclerViewAdapter);
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }
}
