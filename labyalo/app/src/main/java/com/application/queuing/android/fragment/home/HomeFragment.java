package com.application.queuing.android.fragment.home;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.application.queuing.R;
import com.application.queuing.android.activity.HomeActivity;
import com.application.queuing.android.adapter.DefaultRecyclerViewAdapter;
import com.application.queuing.data.model.api.SampleModel;
import com.application.queuing.server.request.Auth;
import com.application.queuing.vendor.android.base.BaseFragment;
import com.application.queuing.vendor.server.transformer.BaseTransformer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HomeFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback, DefaultRecyclerViewAdapter.ClickListener{

    private HomeActivity homeActivity;

    private GoogleMap mMap;
    private DefaultRecyclerViewAdapter defaultRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private SupportMapFragment supportMapFragment;


    @BindView(R.id.queuesRV)                RecyclerView queueRV;

    public static HomeFragment newInstance(){
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewReady() {
        homeActivity = (HomeActivity) getContext();
        setUpRecyclerView();
    }

    public void setUpRecyclerView(){
        defaultRecyclerViewAdapter = new DefaultRecyclerViewAdapter(getContext());
        defaultRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayout.HORIZONTAL, false);
        queueRV.setLayoutManager(linearLayoutManager);
        queueRV.setAdapter(defaultRecyclerViewAdapter);
        defaultRecyclerViewAdapter.setClickListener(this);
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (supportMapFragment == null) {
            supportMapFragment = new SupportMapFragment();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.maps, supportMapFragment).commit();
            supportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.homePanel:
                break;
            case R.id.historyPanel:
                break;
            case R.id.AddQueuePanel:
                break;
            case R.id.creditsPanel:
                break;
            case R.id.profilePanel:
                break;
            default:
                break;

        }

    }

    @Override
    public void onItemClick(SampleModel sampleModel) {
        homeActivity.startQueueActivity("queue");
    }
}
