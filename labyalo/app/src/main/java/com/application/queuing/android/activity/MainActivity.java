package com.application.queuing.android.activity;

import com.application.queuing.R;
import com.application.queuing.android.fragment.DefaultFragment;
import com.application.queuing.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainActivity extends RouteActivity {
    public static final String TAG = MainActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":

                break;
                default:
                    openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
}
