package com.application.queuing.android.fragment.register;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.application.queuing.R;
import com.application.queuing.android.activity.LandingActivity;
import com.application.queuing.server.request.Auth;
import com.application.queuing.vendor.android.base.BaseFragment;
import com.application.queuing.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SignUpFragment.class.getName().toString();

    @BindView(R.id.firstNameET)                         EditText firstNameET;
    @BindView(R.id.lastNameET)                          EditText lastNameET;
    @BindView(R.id.contactNumberET)                     EditText contactNumET;
    @BindView(R.id.streetAddET)                         EditText streetAddET;
    @BindView(R.id.stateET)                             EditText stateET;
    @BindView(R.id.countryET)                           EditText countryET;
    @BindView(R.id.emailET)                             EditText emailET;
    @BindView(R.id.passwordET)                          EditText passET;
    @BindView(R.id.reenterpassET)                       EditText reenterPassET;
    @BindView(R.id.createAccountLL)                     LinearLayout createAccount;


    private LandingActivity landingActivity;


    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_register;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        createAccount.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.createAccountLL:
                landingActivity.startMainActivity("home");
                break;
            default:
        }
    }
}
