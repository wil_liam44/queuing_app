package com.application.queuing.android.activity;

import com.application.queuing.R;
import com.application.queuing.android.fragment.landing.LoginFragment;
import com.application.queuing.android.fragment.register.SignUpFragment;
import com.application.queuing.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LandingActivity extends RouteActivity {
    public static final String TAG = LandingActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                openLoginFragment();
                break;
            default:
                openLoginFragment();
                break;
        }
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }
}
