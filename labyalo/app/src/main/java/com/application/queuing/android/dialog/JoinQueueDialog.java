package com.application.queuing.android.dialog;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.application.queuing.R;
import com.application.queuing.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class JoinQueueDialog extends BaseDialog implements View.OnClickListener {
    public static final String TAG = JoinQueueDialog.class.getName().toString();


    @BindView(R.id.firstQTR)            LinearLayout firstQTR;
    @BindView(R.id.secondQTR)           LinearLayout secondQTR;
    @BindView(R.id.thirdQTR)            LinearLayout thirdQTR;
    @BindView(R.id.fourthQTR)           LinearLayout fourthQTR;
    @BindView(R.id.dialogPriceET)       EditText    priceET;
    @BindView(R.id.dialogTransactioneType)  Spinner transactionSpinner;
    @BindView(R.id.dialogEventName)         TextView eventTV;
    @BindView(R.id.dialogBuy)           LinearLayout buyLL;
    @BindView(R.id.dialogSell)          LinearLayout sellLL;
    @BindView(R.id.firstText)           TextView firstTV;
    @BindView(R.id.secondText)           TextView secondTV;
    @BindView(R.id.thirdText)           TextView thirdTV;
    @BindView(R.id.fourthText)          TextView fourthTV;

    public Callback callback;
    public String quarter = "one";

    public static JoinQueueDialog newInstance(Callback callback){
        JoinQueueDialog joinQueueDialog = new JoinQueueDialog();
        joinQueueDialog.callback = callback;
        return joinQueueDialog;
    }


    @Override
    public int onLayoutSet() {
        return R.layout.dialog_join_queue;
    }

    @Override
    public void onViewReady() {

        firstQTR.setOnClickListener(this);
        secondQTR.setOnClickListener(this);
        thirdQTR.setOnClickListener(this);
        fourthQTR.setOnClickListener(this);
        buyLL.setOnClickListener(this);
        sellLL.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogMatchParent();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.firstQTR:
                changeColors("first");
                break;
            case R.id.secondQTR:
                changeColors("second");
                break;
            case R.id.thirdQTR:
                changeColors("third");
                break;
            case R.id.fourthQTR:
                changeColors("fourth");
                break;
            case R.id.dialogBuy:
                if(callback!=null) {
                    callback.onSuccess(quarter);
                    dismiss();
                }
                break;
            case R.id.dialogSell:
                dismiss();
                break;

        }
    }

    public void changeColors(String type){
        if(type.equalsIgnoreCase("first")){
            firstQTR.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            secondQTR.setBackgroundColor(getResources().getColor(R.color.darkersecondaryBackground));
            thirdQTR.setBackgroundColor(getResources().getColor(R.color.lightersecondaryBackground));
            fourthQTR.setBackgroundColor(getResources().getColor(R.color.darkersecondaryBackground));
            firstTV.setTextColor(getResources().getColor(R.color.white));
            secondTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            thirdTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            fourthTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            quarter = "one";
        }
        else if(type.equalsIgnoreCase("second")){
            firstQTR.setBackgroundColor(getResources().getColor(R.color.lightersecondaryBackground));
            secondQTR.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            thirdQTR.setBackgroundColor(getResources().getColor(R.color.lightersecondaryBackground));
            fourthQTR.setBackgroundColor(getResources().getColor(R.color.darkersecondaryBackground));
            firstTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            secondTV.setTextColor(getResources().getColor(R.color.white));
            thirdTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            fourthTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            quarter = "two";
        }
        else if(type.equalsIgnoreCase("third")){
            firstQTR.setBackgroundColor(getResources().getColor(R.color.lightersecondaryBackground));
            secondQTR.setBackgroundColor(getResources().getColor(R.color.darkersecondaryBackground));
            thirdQTR.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            fourthQTR.setBackgroundColor(getResources().getColor(R.color.darkersecondaryBackground));
            firstTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            secondTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            thirdTV.setTextColor(getResources().getColor(R.color.white));
            fourthTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            quarter = "three";
        }
        else if(type.equalsIgnoreCase("fourth")){
            firstQTR.setBackgroundColor(getResources().getColor(R.color.lightersecondaryBackground));
            secondQTR.setBackgroundColor(getResources().getColor(R.color.darkersecondaryBackground));
            thirdQTR.setBackgroundColor(getResources().getColor(R.color.lightersecondaryBackground));
            fourthQTR.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            firstTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            secondTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            thirdTV.setTextColor(getResources().getColor(R.color.backgroundTextColor));
            fourthTV.setTextColor(getResources().getColor(R.color.white));
            quarter = "four";
        }
    }


     public interface Callback{
        void onSuccess(String sample);
     }
}
