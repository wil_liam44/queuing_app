package com.application.queuing.android.dialog;

import com.application.queuing.R;
import com.application.queuing.vendor.android.base.BaseDialog;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class DefaultDialog extends BaseDialog {
	public static final String TAG = DefaultDialog.class.getName().toString();

	public static DefaultDialog newInstance() {
		DefaultDialog dialog = new DefaultDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_default;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
