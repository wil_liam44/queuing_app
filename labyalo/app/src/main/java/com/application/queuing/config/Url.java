package com.application.queuing.config;

import com.application.queuing.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("https://www.appgradingtechnology.com");
    public static final String DEBUG_URL = decrypt("https://www.appgradingtechnology.com");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;
}
